function getCSS() {
  datetoday = new Date();
  themonth = datetoday.getMonth() + 1;

  if (themonth == 12)
    display = "winter.css";
  else if (themonth <= 2)
    display = "winter.css";
  else if (themonth > 2 && themonth <= 5)
    display = "spring.css";
  else if (themonth > 5 && themonth <= 8)
    display = "summer.css";
  else if (themonth > 8 && themonth <= 11)
    display = "autumn.css";

  document.getElementById('season').href = display;
}
