var path = require('path');
var webpack = require('webpack');
var CommonsChunkPlugin = new require("webpack/lib/optimize/CommonsChunkPlugin");
module.exports = {
  clearBeforeBuild: 'true',
  devtool: 'eval-source-map',
  context: path.resolve('js'),
  entry: {
    //chunck name: "assetname",
    bo: "bootstrap-webpack!./bootstrap.config.js",
    a: "file?name=test.html!jade-html!../public/test.jade", // http://localhost:8080/public/assets/js/test.html
    b: "file?name=grwh.html!jade-html!../public/test2.jade", // http://localhost:8080/public/assets/js/grwh.html
    c: "file?name=testfooter.html!jade-html!../public/test3.jade", // http://localhost:8080/public/assets/js/testfooter.html
    d: "file?name=testfooter2.html!jade-html!../public/test4.jade", // http://localhost:8080/public/assets/js/testfooter2.html
    app4: "./app4.js",
    oapp: "./oapp.js",
    xapp: "./xapp.js",
    Bindex: "./index.js"
  },
  output: {
    path: path.resolve('build/js/'), // bundle.js zal hier terechtkomen
    publicPath: '/public/assets/js/', // webpack result is served from /public/assets/js/ - waar bundle zit op de webserver
    filename: "[name].bundle.js" // default is main.bundle.js
  },
  plugins: [
    new CommonsChunkPlugin("commons.chunk.js"),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }) // je kan ook in .js file dit require('expose?$!expose?jQuery!jquery');
  ],
  //  devServer: {
  //    contentBase: 'public' // content is served from public - index.html moet uit deze directory komen
  //  },
  devServer: {
    contentBase: 'public',
    host: '0.0.0.0',
    port: 8080,
    stats: {
      colors: true
    },
  //  watchOptions: {
  //    aggregateTimeout: 50,
  //  },
  },
  devtool: 'source-map',
  module: {
    loaders: [{
      test: /bootstrap\/js\//,
      loader: 'imports?jQuery=jquery'
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader"
    }, {
      test: /\.html$/,
      exclude: /node_modules/,
      loader: "raw-loader"
    }, {
      test: /\.jade$/,
      exclude: /node_modules/,
      loader: 'jade-html'
    }, {
      test: /\.css$/,
      loaders: ["style", "css"]
    }, {
      test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=application/font-woff"
    }, {
      test: /\.woff(\?v=\d+\.\d+\.\d+)?2$/,
      loader: "url?limit=10000&mimetype=application/font-woff"
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=application/octet-stream"
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: "file"
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=image/svg+xml"
    }, {
      test: /\.(png|jpg|jpeg|gif|woff)$/,
      loader: 'url-loader?limit=8192'
    }, {
      test: /\.styl$/,
      loaders: ["style", "css", "stylus"]
    }]
  },
  resolve: {
    extensions: ['', '.js', '.es6', '.jade']
  }
};
